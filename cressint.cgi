#!/usr/bin/perl -w
########################################################################
##                                                                    ##
##   Thal-Int's Perl CGI for processing form inputs                   ##
##                                                                    ##
##   CGI script to submit Thal-Int jobs to LSF on the BMI cluster     ##
##                                                                    ##
##   Authors:  M. Borowczak, S. Dixit, K. Ernst, et al.               ##
##                                                                    ##
########################################################################
use strict;
use warnings;

use lib qw( /var/www/lib/perl );
use local::lib '/var/www/perl5-local-lib/';
use vars qw( $DEFAULT_ERR_CODE $CMD_TEMPLATES $CMD_TEMPLATE $MAX_UPDOWN_BASES
             $LEGAL_JOBID $LEGAL_CHARACTERS $FIELD_CHAR_LIMIT );
#use autodie;
use English;                       # use English names for punctuation vars
use File::Spec::Functions;         # for catfile and catdir
use File::Copy 'cp';
use File::Path 'remove_tree';      # for 'remove_tree'
use Config::Tiny;                  # for reading bin/data paths from .ini file
use Data::Dump qw(dd dump);        # for debugging (NB: 'dump' goes to STDERR)
use Email::Valid;                  # validating RFC-822-compliant email addr
use Template;                      # for rendering HTML confirmation
use Capture::Tiny qw(capture);     # for capturing stdout/stderr of shell cmd
use CGI;                           # the CGI request object
use CGI::Carp qw(set_die_handler); # Trap abnormal exits with a handler
use HTML::Scrubber;                # used to sanitize input, preventing XSS
use WRL::ConfigParser;
use WRL::ConfigParser::Template;

#use CGI::Carp 'fatalsToBrowser';
# Valuable nugget from the documentation:
#   If you use set_die_handler(), you will most likely interfere with the
#   behavior of fatalsToBrowser, so you must use this or that, not both.

$DEFAULT_ERR_CODE = 500;
$MAX_UPDOWN_BASES = 10000;
# FIXME: I should be using a library, not doing this kind of validation on
# my own.
$LEGAL_JOBID      = '^[0-9a-f]{10}';
$LEGAL_CHARACTERS = '[-+_@#()?!.:A-Za-z0-9 ]';
$FIELD_CHAR_LIMIT = 255;
$CGI::POST_MAX    = 50 * 1048576; # megabytes

sub is_between;
sub is_safe_text;
sub bad_form_input;
sub create_bsub_batch;

BEGIN {
    sub emit_error {  ## no critic
        my ($msg, $code) = @_;
        my %CODES = ( 400 => '400 Bad Request',
                      500 => '500 Internal Server Error',
                      501 => '501 Not Implemented',
                    );

        if (defined($code)) {
            $code = $DEFAULT_ERR_CODE unless defined($CODES{$code});
        } else {
            $code = $DEFAULT_ERR_CODE;
        }

        print "Content-type: text/plain\nStatus: $CODES{$code}\n\n";
        if (defined($msg)) {
            chomp($msg);
            print $msg;
        }

        # If you forget this part, weird things happen because the code keeps
        # running. If you 'exit 1' here, you'll probably get an "Error 500".
        exit;
    }
    set_die_handler(\&emit_error);
} # BEGIN

my $TOOL_NAME         = 'cressint';
my $CONFIG_INI        = "$TOOL_NAME.ini";
my $TMPL_INCLUDE_PATH = '/var/www/templates';
my $TT_TEMPLATE       = 'jobsubmit_fragment.html.tt';
my $INPUT_FILE        = 'input.bed';
my $TRACK_LIST        = 'track.lst';
my $CELL_LINE_LIST    = 'cell.lst';
my $PHENO_LIST        = 'phenotypes.lst';
my $BATCH_FILENAME    = 'bsub_job.bat';
my $CLUSTER_IFACE     = '/var/www/cluster_interface/lsf-connector.sh';
my $PREPAREJOB_SH     = '/var/www/cluster_interface/lsf-prepare-job.sh';
my $PAD_BED6          = '/data/weirauchlab/local/web-bin/pad_to_BED6.pl';
my $GENE2BED_CMD      = '/data/weirauchlab/local/web-bin/Gene2Bed';

# Decide which script template to use (below) based on the mode selector
# This is a hash REFERENCE, not a hash. Keep that in mind.
my $CMD_TEMPLATES     = { 'mode1' => 'mode1.sh.tmpl',
                          'mode2' => 'mode2.sh.tmpl',
                          'mode3' => 'mode3.sh.tmpl' };


# --------------------------------------------------------------------
#                        CGI Session Setup
# --------------------------------------------------------------------
$CGI::POST_MAX = 1024 * 1024 * 50; # MB
my $cgi = new CGI();

# See http://www.perlmonks.org/?node_id=1105051 for a discussion of what
# happens when you try to use param() (instead of multi_param()) in a list
# context. Suffice it to say: odd things happen.
(my $job_id       = $cgi->param('formkey')) =~ s/^($LEGAL_JOBID).*/$1/;
my $mode          = $cgi->param('mode-selector'); # { 'mode1' | 'mode2' ... }
my $input_type    = $cgi->param('input-type-selector'); #coords/gene-names/fasta
my $coords        = $cgi->param('coords');
# (temporarily) disabled for security review
my $coords_file   = undef; # $cgi->param('coords-file');
my @phenotypes    = $cgi->multi_param('phenotypes');
my @lines         = $cgi->multi_param('selected-lines');
my @tracks        = $cgi->multi_param('selected-tracks');
my $scan_promoter = $cgi->param('scan-promoter');
my $up_bases      = $cgi->param('up-bases');
my $down_bases    = $cgi->param('down-bases');
# (temporarily) disabled for security review
my $email         = undef; # $cgi->param('email');
#my $username     = $cgi->param('username'); # use default from .ini
my $job_name      = $cgi->param('jobname');
my $validate      = $cgi->url_param('validate') ? 1 : 0;


# --------------------------------------------------------------------
#                Form Input Validation / Sanitizing
# --------------------------------------------------------------------
# Strip *all* HTML tags
my $scrubber = HTML::Scrubber->new();

# Test for key CGI parameters and bail if we don't get them. Multi-selects are
# a bit tricky because CGI.pm seems to return them as an array consisting of a
# single empty string if none of the options are selected.

# Something is really wrong if we're missing the job ID.
bad_form_input('missing', 'job-id')          unless $job_id;
bad_form_input('missing', 'mode-selector')   unless $mode;
bad_form_input('missing', 'selected-lines')  unless $lines[0];
bad_form_input('missing', 'selected-tracks') unless $tracks[0];
bad_form_input('invalid', 'jobname')         unless is_safe_text($job_name);
bad_form_input('invalid', 'email')           unless is_safe_text($email);
bad_form_input('invalid', 'up-bases')
    unless is_between($up_bases, 0, $MAX_UPDOWN_BASES);
bad_form_input('invalid', 'down-bases')
    unless is_between($down_bases, 0, $MAX_UPDOWN_BASES);

if (!$coords and !$coords_file and !$phenotypes[0]) {
    bad_form_input('missing', 'coords', 'phenotypes');
    #emit_error("No input coordinates or gene names were supplied.", 400);
}

if ($phenotypes[0] and ($coords or $coords_file)) {
    emit_error("Genomic coordinates and phenotype may not be specified at "
               . "the same time." , 400);
}

# Strip out tags from any user input
# TODO: do the same filtering for an uploaded file
$coords = $scrubber->scrub($coords);

if ($validate) {
    print $cgi->header();
    print $cgi->start_html();
    print $cgi->pre($cgi->Dump());
    print $cgi->end_html();
    exit;
}

# Store this off here, since that's one less package global that will need to
# be handed off to create_bsub_batch() below
my $CMD_TEMPLATE = "scripts/$CMD_TEMPLATES->{$mode}";


# --------------------------------------------------------------------
#        Read config values like data path from the .ini file
# --------------------------------------------------------------------
my $cfg = WRL::ConfigParser->read( $CONFIG_INI,
                                   -extratoks => { 'job_id', $job_id } );

# Handles to config settings, just for convenience
# FIXME: We should bomb here if *any* of these aren't defined in the .ini
# instead of hard coding defaults in eight places
my $proper_name   = $cfg->{global}->{proper_name};
my $job_dir       = $cfg->{$TOOL_NAME}->{output_path};
my $job_props     = $cfg->{global}->{job_props} || 'job_properties.txt';
my $bsub_log_path = $cfg->{cluster}->{bsub_log_path};
my $default_email = $cfg->{global}->{admin_email} ||'tftoolsadmin@cchmc.org';
my $username      = $cfg->{global}->{default_user} ||'ern6xv';
my $CRESS_GENES   = "$cfg->{$TOOL_NAME}->{pbmdata_path}/Arabidopsis_Gene.bed";

### Error and Messaging Variables
my $RESULTS      = "/$TOOL_NAME/results/$job_id";
my $cmdline      = ''; # a temp variable to build up a command line for execution

# --------------------------------------------------------------------
#        Substitute defaults if not provided by the CGI form
# --------------------------------------------------------------------
if (!defined($cfg->{$TOOL_NAME}->{default_up_bases}) or
    !defined($cfg->{$TOOL_NAME}->{default_down_bases})) {
    die "Default upstream/downstream bases missing from configuration file\n";
}
$up_bases   ||= $cfg->{$TOOL_NAME}->{default_up_bases};
$down_bases ||= $cfg->{$TOOL_NAME}->{default_down_bases};
$job_name   ||= "$proper_name job #$job_id";

my $GENE2BED_ARGS = ($scan_promoter ? '-promoter ' : '')
                  . "-upstream $up_bases -downstream $down_bases";

if (!$email) {
    $email = $default_email;
} else {
    # Append @cchmc.org if the user left that part off (and we told them they
    # could). No, I don't trust Exchange to figure it out.
    $email =~ s/^(\w+\.?\w+)$/$1\@cchmc.org/;

    # See if what we got back was a valid email:
    $email = Email::Valid->address($email);
    $email ||= $default_email;
} # if no email provided


# --------------------------------------------------------------------
#          Prepare the cluster submission using form inputs
# --------------------------------------------------------------------

# This substitutes form values and configuration settings into the
# command template for invoking CressInt
$cmdline = WRL::ConfigParser::Template->expand(
    $CMD_TEMPLATE, $cfg,
    scalar $cgi->Vars(), # so CGI.pm gives us a hashref
    { 'job_id'    => $job_id,
      'tool_name' => $TOOL_NAME }
);

# FIXME: Kludge around the weird problem with 'remove_tree' not removing the
# parent during the last invocation.
if ( -d $job_dir && !glob("$job_dir/*") ) {
    rmdir $job_dir or die "Error removing old (empty) job dir ($OS_ERROR)\n";
}

# Prepare the output directories
mkdir $job_dir       or die "Error creating job directory ($OS_ERROR)\n";
chdir $job_dir       or die "Couldn't 'cd' to job directory ($OS_ERROR)\n";
mkdir $bsub_log_path or die "Couldn't create bsub log directory ($OS_ERROR)\n";

open my $JOBPROPS, '>', $job_props or die "$OS_ERROR opening $job_props\n";
print {$JOBPROPS} "$job_name ($mode)\n" or die "$OS_ERROR writing $job_props\n";
# Don't forget to close this file later on!

# If we got a filename, ignore the 'coords' textarea and the phenotypes select
# box
if ($coords_file) {
    # Copy the input file (list of coordinates) to the job directory:
    cp $cgi->tmpFileName($coords_file), $INPUT_FILE or die $OS_ERROR;
} elsif ($mode ne 'mode3') {
    # Unless 'mode3' was selected (SNP-to-TFBS), write the contents of the
    # 'dirlist' text area to the output file:
    open my $INPUT, '>', $INPUT_FILE or die $OS_ERROR;

    # This should preclude having to call `dos2unix` all over the place.
    my @coords = split(/\r?\n/, $coords);
    foreach my $coord (@coords) {
        # Skip comments (which ConfigParser::Template would otherwise allow)
        next if $coord =~ /^#/;
        print {$INPUT} "$coord\n" or die "Problem writing output file ($OS_ERROR)\n";
    }
    close $INPUT;
} # if form field 'inputfile' wasn't empty

# Pad BED file input to 6 columns (return an Error 400 if there's a problem)
if ($mode =~ /mode[12]/ and $input_type eq 'coords') {
    # Pad the input BED file to six columns ('pad_to_BED6.pl' returns an error
    # if the file is malformed). The '-b' option makes a backup (.bak).
    my $cmd = "\"$PAD_BED6\" -b -o \"$INPUT_FILE\" &>/dev/null";
    # 'system()' returns a non-zero value if it *failed*:
    my $retval = system($cmd);

    if ($retval) {
        # Clean up after ourselves so user can re-submit without having to
        # reload the page (can re-use the same job ID generated by the input
        # form):
        chdir '..';
        # FIXME: This doesn't remove the parent. Bug in the library?
        remove_tree($job_dir, {safe=>1})
            or warn "Error removing job directory ($OS_ERROR)\n";
        emit_error("Problem parsing input BED file", 400);
    }
} # process BED file input for Modes 1 and 2

# Write out the list of selected cell types (experiments) and tracks.
open my $LINES, '>', $CELL_LINE_LIST or die $OS_ERROR;
foreach my $line (@lines) {
    print {$LINES} "$line\n" or die "Problem writing cell line list ($OS_ERROR)\n";
}
close $LINES;

open my $TRACKS, '>', $TRACK_LIST or die $OS_ERROR;
foreach my $track (@tracks) {
    print {$TRACKS} "$track\n"or die "Problem writing track list ($OS_ERROR)\n";
}
close $TRACKS;

if ($mode eq 'mode3') {
    open my $PHENOS, '>', $PHENO_LIST or die $OS_ERROR;
    foreach my $pheno (@phenotypes) {
        print {$PHENOS} "$pheno\n"
            or die "Problem writing phenotype list ($OS_ERROR)\n";
    }
    close $PHENOS;
}

# --------------------------------------------------------------------
#             Prepare a command line for invoking CressInt
# --------------------------------------------------------------------

# Create a batch file for the cluster submission.
# FIXME: This should be updated to use the methods provided by WRL::ClusterJob
create_bsub_batch( $cfg, $cgi, $job_id, $BATCH_FILENAME, $job_name, $job_dir,
                   $bsub_log_path, $cmdline, $email );

# Yes, it's in /etc/sudoers
$cmdline = <<"END_CMD";
sudo $CLUSTER_IFACE '$username' bsub '$job_dir' '$PREPAREJOB_SH' '$BATCH_FILENAME'
END_CMD

# Using system() and attempting to capture its output in external files is
# EXTREMELY fussy. Use Capture::Tiny instead. Saves two temp files and
# associated cleanup.
my ($cmdout, $cmderr) = capture {
    system($cmdline);
};

$cmderr ||= 'Good news: looks like the job got submitted without errors.';
$cmdout ||= 'No messages.';

# Parse $cmdout for the LSF job number:
$cmdout =~ /.*Job <(\d+)> is submitted.*/;
# Write that to 'job_name.txt'
print {$JOBPROPS} "$1\n" or
    warn "Couldn't write LSF submission ID to $job_props ($OS_ERROR)";
close $JOBPROPS or warn $OS_ERROR;

# --------------------------------------------------------------------
#                     Return the result as HTML             
# --------------------------------------------------------------------
print $cgi->header();

# Initialize Template Toolkit (used to render HTML):
my $template = Template->new({
    INCLUDE_PATH => $TMPL_INCLUDE_PATH,
});

die "ACK! Template '$TT_TEMPLATE' not present on filesystem.\n"
    if ! -f catfile($TMPL_INCLUDE_PATH, $TT_TEMPLATE);

# Otherwise, render the template with the values we have:
$template->process($TT_TEMPLATE, {
    job_id      => $job_id,
    proper_name => $proper_name,
    tool_name   => $TOOL_NAME,
    back_name   => $proper_name,
    back_url    => "/$TOOL_NAME",
    results_url => $RESULTS,
    errors      => $cmderr,
    messages    => $cmdout,
    year        => (localtime)[5] + 1900,
});

exit;


# --------------------------------------------------------------------
#                      S U B R O U T I N E S
# --------------------------------------------------------------------

# Report if a number is between two others:
sub is_between {
    my ($n, $lower, $upper) = @_;
    # http://www.perl.com/doc/FMTEYEWTK/is_numeric.html
    return 0 if $n !~ /^([+-]?)(?=\d|\.\d)\d*(\.\d*)?([Ee]([+-]?\d+))?$/;
    return 0 if $n < $lower or $n > $upper;
    return 1;
}

# Report if given text input contains "unsafe" characters:
sub is_safe_text {
    my $text = shift;
    return 1 unless $text; # allow the empty string
    return 0 if length($text) > $FIELD_CHAR_LIMIT;
    return 0 if $text !~ /^$LEGAL_CHARACTERS+$/;
    return 1;
}

# Emit an error message (with an appropriate HTTP status code) if a form input
# is discovered to be conspicuously absent from the CGI environment:
sub bad_form_input {
    my ($problem, $list, @rest) = @_;

    # Comma-separate multiple fields
    if (length(@rest)) { foreach my $f (@rest) { $list = "$f, $list"; } }

    if ($problem eq 'invalid') {
        emit_error("Invalid/out-of-range form input(s): $list", 400);
    } elsif ($problem eq 'missing') {
        emit_error("Missing form input(s): $list", 400);
    }
} # bad_form_input


# Read in parameters like job name and the command and generate output that's
# formatted as a bsub batch job submission.
sub create_bsub_batch {
    my ($cfg, $cgi, $jobid, $batchfile, $job_name, $job_dir, $cluster_logpath,
        $cmd, $email) = @_;

    # FIXME: This is fragile, and heavily dependent on BMI's proxying setup
    my $server = $cgi->http('HTTP_X_FORWARDED_SERVER') || $cgi->server_name();
    my $url = "https://$server/$TOOL_NAME/results/$jobid";

    # Strip double-hashed comments and empty lines from $cmd. Leave other
    # comments intact.
    my @cmds = split(/\r?\n/, $cmd);
    $cmd = join("\n", grep(!/(^#\s+|^\s+$)/, @cmds));

    my $job;
    my $bsub_reservation = '';
    if ($cfg->{cluster}->{bsub_reserv_id}) {
        # Don't use the LSF '-U' option unless we're sure this is a valid
        # reservation. Check with 'brsvs'.
        my $reservs = `brsvs -w`;
        if ( $reservs =~ /^$cfg->{cluster}->{bsub_reserv_id}/m ) {
            $bsub_reservation = "#BSUB -U $cfg->{cluster}->{bsub_reserv_id}";
        }
    }
    
    ($job = <<"        JOB_END") =~ s/^ {8}//gm;
        #!/bin/bash
        #BSUB -n $cfg->{cluster}->{bsub_cpu_cores}
        #BSUB -M $cfg->{cluster}->{bsub_mem_limit}
        #BSUB -W $cfg->{cluster}->{bsub_wall_time}
        $bsub_reservation
        #BSUB -e \"$cluster_logpath/job%J.err\"
        #BSUB -o \"$cluster_logpath/job%J.out\"
        #BSUB -J \"$job_name\"
        #BSUB -N
        #BSUB -u \"$email\"

        # Results can be viewed here:
        # --------------------------
        # $url

        test -d "$job_dir" || exit 1
        cd "$job_dir"
        JOB_END

    # If the #input-type-selector radio is set to "gene-names", then call
    # Gene2Bed to convert gene names back into coordinates. The input file
    # already comes stripped of carriage returns, so no need for `dos2unix`.
    if ($mode =~ /mode[12]/ and $input_type eq 'gene-names') {
        ($job .= <<"        JOB_END") =~ s/^ {12}//gm;

            module load gcc/4.8.1
            $GENE2BED_CMD $INPUT_FILE $CRESS_GENES $GENE2BED_ARGS
            cp input.bed genes.lst
            mv -f region.bed input.bed || exit 1
        JOB_END
    }

    ($job .= <<"        JOB_END") =~ s/^ {8}//gm;

        # Generated from '$CMD_TEMPLATE'
        $cmd
        JOB_END

    #return $job;
    open my $BATCHFILE, '>', $batchfile
        or die "Problem creating LSF batch file ($OS_ERROR)\n";
    print {$BATCHFILE} $job or die "Problem writing to batch file ($OS_ERROR)\n";
    close $BATCHFILE;
} # sub create_bsub_batch

# CressInt's 'submit.cgi'
# vim: tags+=/var/www/lib/perl/WRL/tags
