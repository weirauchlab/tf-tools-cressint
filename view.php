<?php
// FIXME: This tool should also probably just be an Ajax call from 'rlist.php'

require_once('../../lib/php/WRL/tfconfig.php');
require_once('../../lib/php/WRL/tabtable.php');
require_once('../../lib/php/WRL/tftemplate.php');
require_once('../../lib/php/WRL/tfclusterjob.php');
require_once('../../lib/php/WRL/tfcache.php');

global $IS_DEV;
$IS_DEV = ( getenv('TF_TOOLS_ENV')==='dev' ? true : false );

// Sanitize inputs to ward off some kind of creative filesystem exploration 
// with crafted job_ids or tool_names:
$job_id    = preg_replace('/[^a-f0-9]/', '', $_GET['j']);
$tool_name = preg_replace('/[^a-zA-Z0-9_-]/', '', $_GET['t']);
$basename  = '';
$cfg       = NULL;
$matches   = NULL;
$errmsg    = '';


if ( isset($_SERVER['HTTP_REFERER']) ) {
    $referrer = $_SERVER['HTTP_REFERER'];
} else {
    // FIXME: this will bomb on the dev server
    $referrer = "https://tf.cchmc.org/$tool_name/display.php?jid=$job_id";
}

// Read in the config file 
tf_read_config($cfg, "${tool_name}.ini", array('job_id' => $job_id)) or
    bail("Problem reading ${tool_name}.ini");

$output_dir = $cfg[$tool_name]['output_path'];
$filename   = $cfg[$tool_name]['output_path'];  // initially
$log_dir    = $cfg['cluster']['bsub_log_path'];

if (array_key_exists('f', $_GET)) {
    $basename = $_GET['f'];
    // Munge the path a little bit if the calling script asked for the LSF 
    // output:
    if (preg_match('/^bsub_(\w{3})$/', $basename, $matches)) {
        $filename = preg_fetch_cluster_logs($log_dir, $matches[1], $errmsg);
        if ($filename === false) { tf_bail($errmsg); }
        $basename = basename($filename);
    }
    else {
        // Sanitize (FIXME: this is kind of pointless if the file doesn't end 
        // up existing anyway; might be better to bail early.)
        $basename = preg_replace('/[^a-zA-Z0-9._-]/', '', $basename);
        $filename .= "/$basename";
    } // if they asked for LSF output

} else {
    // Take the first basename from 'output_files' in the .ini
    // FIXME: This code is duplicated in 'display.php' (line 128-ish)
    $filelist = preg_split('/\s*,\s*/', $cfg[$tool_name]['output_files']);
    $basename  = array_shift($filelist);
    $filename .= "/$basename";
} // if 'f=whatever' was present in the request options


if (is_file($filename)) {
    $hasheader = array_key_exists('hasheader', $_GET);

    // If we got the 'render' parameter, and NOT download parameter, and 
    // filetype was NOT PDF, then render as an HTML table.
    if ( array_key_exists('render', $_GET) && 
         !array_key_exists('download', $_GET) && 
         substr($basename, -4) !== '.pdf' ) {

        if ($basename === 'PBMresults.bed') {
            $purge      = array_key_exists('purge', $_GET);
            $with_links = "$output_dir/PBMresults.html";
            // If we can parse the PBMresults file into links to CisBP (i.e., 
            // no errors from the database query), then use that file instead
            if( parse_cisbp_links($filename, $with_links, $purge) ) {
                $filename  = $with_links;
                $hasheader = true;
            }
        }

        // Most files won't have a header row, but let the user force one if she
        // really wants one:
        render_table( $filename, array('toolname'  => $tool_name,
                                       'hasheader' => $hasheader) );
        exit;
    } // '&render' request param

    if (substr($basename, -4) === '.pdf' || substr($basename, -4) === '.PDF') {
        header('Content-Type: application/pdf');
    } else {
        header('Content-Type: text/plain');
    } // if it's a PDF, serve it as a PDF

    // If 'render' was also specified, ignore both of them.
    if (array_key_exists('download', $_GET) &&
        !array_key_exists('render', $_GET)) {
        // Add a '.txt' extension if it doesn't have one already
        if (substr($basename, -4) !== '.txt') { $basename .= ".txt"; }
        header("Content-Disposition: attachment; filename=\"$basename\";");
    } // '&download' request param

    header('Content-Transfer-Encoding: binary');
    header('Content-Length: ' . filesize($filename));
    readfile($filename);

} else {

    // FIXME: This isn't visible in the client.
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
    header("Refresh: 5; $referrer");
    tf_bail(<<<"HTML"
      <p>The filename <tt>$basename</tt> could not be found in
         the output directory for <tt>$tool_name</tt>.</p>
      <p>Returning you to the results page in 5 seconds, or
         <a href="$referrer">click here</a>.</p>
HTML
    );

} // if the file exists (or doesn't)

// FIXME: Right here in view.php maybe isn't the best place for this
function parse_cisbp_links($infile, $outfile, $purge = false) {

    // If the output file already exists and it's newer than the input file, 
    // AND the user didn't ask to purge the cached version, don't bother 
    // writing another one. Bail out.
    if (filesize($outfile) > 0 && tf_file_is_newer($outfile, $infile) &&
        $purge === false) {
        return true;
    }
    
    global $IS_DEV;
    // Bring credentials in from an external file (*not* checked in to Git)
    require_once('dbcredentials.inc');
    $TABLE    = 'motif_id_map';
    $URL_BASE = 'http://cisbp.ccbr.utoronto.ca/TFreport.php?searchTF=';
    $motifcol = 7;
    $dblink = mysqli_connect($DBHOST,$DBUSER,$DBPASS,$DB) or die(mysql_error());
    $ifh = fopen($infile, 'r')  or die("Error opening PBMresults input file.");
    $ofh = fopen($outfile, 'w') or die("Error creating CisBPified output file.");

    if ($ifh === false) {
        fclose($ifh);
        fclose($ofh);
        mysqli_close($dblink);
        return false;
    }

    // FIXME: Should just modify the C++ binary instead of kludging this in
    // here, but since we're already here, make the header row look prettier:
    $headerrow = preg_replace( '/(\w)\(/', '$1 (', fgets($ifh) ); 
    $headerrow = preg_replace( '/([a-z0-9])([A-Z])/', '$1 $2', $headerrow );
    $headerrow = preg_replace( '/(Dist|Dir)2/', '$1 to', $headerrow);
    $headerrow = str_replace( '_', ' ', $headerrow );
    fwrite($ofh, $headerrow);

    // Otherwise, read each line from the input file and create an output file 
    // where all the "PWM" columns have been replaced with links to CisBP:
    while (($data = fgetcsv($ifh, 10000, "\t")) !== false) {
        $motif = $data[$motifcol];
        $query = "select tf_id from $TABLE where motif_id='$motif' limit 1;";
        $rs = mysqli_query($dblink, $query);

        // Don't make a link if we couldn't find the Motif ID
        if ($rs && mysqli_num_rows($rs) > 0) {
          $row = mysqli_fetch_array($rs, MYSQL_ASSOC);
          $data[$motifcol] = '<a href="' . $URL_BASE . $row['tf_id']
                           . '" target="_blank" title="View PWM WebLogo '
                           . "for TF ID " . $row['tf_id'] . ' at CisBP">'
                           . $data[$motifcol] . '</a>';
        } // if we got results

        fwrite($ofh, implode("\t", $data) . "\n");
    } // file fgetscv()

    fclose($ifh);
    fclose($ofh);
    mysqli_close($dblink);
    return true;

} // parse_cisbp_links

// vim: sw=4 ts=4 tags+=/var/www/lib/php/WRL/tags
