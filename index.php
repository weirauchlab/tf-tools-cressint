<?php
// ========================================================================
//  index.php
//
//  Arabadopsis thaliana (thale cress) Intersector
//  https://tfwiki.cchmc.org/wiki/CressInt
//
//  Author:   ern6xv (Kevin.Ernst -at- cchmc.org)
//  Date:     02 January 2015
// ========================================================================

require_once('/var/www/lib/php/WRL/tfinit.php');
require_once('/var/www/lib/php/WRL/tftemplate.php');
require_once('/var/www/lib/php/WRL/tfconfig.php');

// Create a pseudo-random 10 hex digit "key" to identify this form submission 
// and the "job ID" in the output folder
$form_key = tf_form_key();
// Look for the config file in the c.w.d. (guesses 'cressint.ini' and tries a 
// couple of other options before giving up)
$ini_file = tf_find_ini();

// Ordinarily, tf_template_init_from_ini() would take care of most of this 
// ugliness for you, but we want a copy of the config array to do other stuff 
// with below.
if ( $ini_file === false ) {
  tf_bail("Problem reading INI file and/or rendering index."); 
}

$cfg = NULL;
tf_read_config($cfg, $ini_file, array( 'job_id' => $form_key ));

if ( !tf_template_init_from_cfg($cfg) ) {
  tf_bail("Problem initializing Twig with config file \"$ini_file\"."); 
}


// Read in the list of phenotypes from an external file and create the
// array we'll need to populate the inputform.html.twig template. The
// "GWAS_phenotype_list*.txt" files are tab-delimited, with the item to be 
// show in the select <option> as the first field and a description to be 
// shown as a tooltip as the second.

$cr_pheno_short = "GWAS_phenotype_list_desc.txt";
$cr_pheno_full  = "GWAS_phenotype_list_FULL_desc.txt";
$cr_pheno_list = $cfg['cressint']['meta_path'] . "/$cr_pheno_short";
#echo "\$cr_pheno_list = $cr_pheno_list";

global $TWIGVARS;
if (!array_key_exists('default_up_bases', $cfg['cressint']) or 
    !array_key_exists('default_down_bases', $cfg['cressint'])) {
    tf_bail("Config file is missing upstream/downstream defaults.");
}
$TWIGVARS['defaultupbases']   = $cfg['cressint']['default_up_bases'];
$TWIGVARS['defaultdownbases'] = $cfg['cressint']['default_down_bases'];

// Split the phenotype list into an array of two-element arrays:
$TWIGVARS['phenotypes'] = file($cr_pheno_list, FILE_IGNORE_NEW_LINES);
array_walk($TWIGVARS['phenotypes'], function(&$listent) {
  $listent = explode("\t", $listent);
});

// FIXME: Cache the results of traversing the filesystem into an external 
// file. Only read the filesystem if the 'basedata_path' has been modified 
// more recently than the cache file was created.
#$TWIGVARS['tracks'] = file('tracks.lst', FILE_IGNORE_NEW_LINES);

// Build up the list of available tracks by enumerating the subfolders of 
// 'basedata_path' from the INI.
$datadir = $cfg['cressint']['basedata_path'];
if (false === ($dh = opendir($datadir)) ) {
  tf_bail("Problem opening %basedata_path%");
}

$TWIGVARS['tracks'] = array();
while (false !== ($ent = readdir($dh)) ) {
  // Skip '.', '..', files, anything beginning with underscores or periods:
  if ( preg_match('/^[._]/', $ent) === 1 ) { continue; }
  // Skip data associated w/ genome-wide association studies (phenotype seq's)
  if ( strpos($ent, 'GWAS') === 0 )        { continue; }
  // Skip files (we want an enumeration of subfolders):
  if ( is_file("$datadir/$ent") )          { continue; }
  // Append anything else
  $TWIGVARS['tracks'][] = $ent;
}  
closedir($dh);

// Build up the list of available cell lines by enumerating subfolders of each 
// of the available tracks (ignoring duplicates):
$TWIGVARS['lines'] = array();
foreach ($TWIGVARS['tracks'] as $trackdir) {
  if (false === ($dh = opendir("$datadir/$trackdir")) ) {
    tf_bail("Problem opening track directory '$track'");
  }
  while (false !== ($ent = readdir($dh)) ) {
    // Skip '.', '..', files, anything beginning with underscores or periods:
    if ( preg_match('/^[._]/', $ent) === 1 )  { continue; }
    // Skip files (we want an enumeration of subfolders):
    if ( is_file("$datadir/$trackdir/$ent") ) { continue; }
    if ( $ent === 'Any' )                     { continue; }
    // Append anything else (we'll filter for uniques later)
    $TWIGVARS['lines'][] = $ent;
  } // enumerate all lines in $trackdir, skipping hidden files, '_' prefixes
  closedir($dh);
} // for each track in 'basedata_path'

sort($TWIGVARS['lines']);
$TWIGVARS['lines'] = array_unique($TWIGVARS['lines']);

// Prepend 'Any':
array_unshift($TWIGVARS['lines'], 'Any');

// Now render the index from the 'index.html.twig' template, using the 
// $TWIGVARS that tf_template_init_from_cfg() created and that we added to 
// above.
tf_render_index();

// vim: tags+=/var/www/lib/php/WRL/tags
