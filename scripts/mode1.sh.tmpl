# LSF script template for the "BedInt" mode of operation (Mode 1) for
# CressInt.
##
## Double-hashed comments will be stripped out from this template, and won't
## appear in the bsub_job.bat job saved in the job output directory.
##
## This template is parsed by WRL::ConfigParser::Template and expects "tokens"
## to look like these examples:
##
## %ff:param%  ==> input value from a form field having id="param"
## %fff:param% ==> input value from a form field, written to a file (param.txt)
## %fft:param% ==> input value from form, substituting tokens from .ini file
## %ini:param% ==> config setting from the tool's .ini file (toolname.ini)
##
# The full pathname to the BED files for Arabidopsis thaliana
TAIRDATA="%ini:basedata_path%"
# Add the external tools ("opt") binaries to the path:
LABROOT="/data/weirauchlab"
PATH=$PATH:$LABROOT/local/web-bin

# Remove Windows CRs (\r) from the input file.
dos2unix -q input.bed

## From $LABROOT/web/bin/BedIntersector.cpp, line 19-ish
## argv[1] bed file,
## argv[2] cell type file,
## argv[3] data type file,
## argv[4] output location with default "-d",
## argv[5],argv[6] output options, e.g., -wa -wb
## That trailing '/' seems to be pretty important.
# Intersect BED files for selected tracks/tissue types with the input
%ini:bedint_bin% input.bed cell.lst track.lst -d "$TAIRDATA/"
##              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
## The files input.bed, cell.lst, and track.lst are created by the Perl CGI and
## contain the (hopefully sanitized) form field inputs from the user.

# Merge results of all BEDtools 'intersectBed' runs (output file:
# 'allresults.bed')
MergingTair10 cell.lst track.lst

# Report information on the gene nearest each intersection:
if [ -s allresults.bed ]; then
    # Trim accession IDs and sequence information from the default results
    # file (retained as "allresults_allcols.bed"):
    mv allresults.bed allresults_allcols.bed
    cut -f1-14 allresults_allcols.bed > allresults.bed

    mv allresults.bed allresults_no_anno.bed
    nearby_gene_anno allresults_no_anno.bed allresults.bed
fi
## vim: ft=sh
