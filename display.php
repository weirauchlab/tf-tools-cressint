<?php
// ====================================================================
//
//   CressInt's display.php
//
//   Pick up data files from completed runs of the CressInt tool and
//   display the results in the browser.
//
//   Authors: Borowczak, M., Dixit, S., Ernst, K.
//
// ====================================================================

//ini_set('display_errors', 'On'); // uncomment for debugging in-browser
require_once('../../lib/php/WRL/tfconfig.php');
require_once('../../lib/php/WRL/tfcallouts.php');
require_once('../../lib/php/WRL/tfcache.php');
require_once('../../lib/php/WRL/tftemplate.php');
require_once('../../lib/php/WRL/tfutil.php');       // for pad_table_row()
require_once('../../lib/php/WRL/tfclusterjob.php'); // for tf_get_last_job_id()

global $TWIG;
global $TOOLNAME; $TOOLNAME = 'cressint';
global $TEMPLATE; $TEMPLATE = 'display.html.twig';
global $INI_FILE; $INI_FILE = $TOOLNAME . '.ini';

$IS_DEV = getenv('TF_TOOLS_ENV')=='dev' ? true : false;

// Update this if 'PBMresults.bed' shows up in the output list
$HAVE_PBMRESULTS = false;

// ====================================================================
//                    I n i t i a l i z a t i o n        
// ====================================================================
//
// Load config settings from the .ini file for this tool
$cfg   = NULL;

if (!array_key_exists('jid', $_GET)) {
    tf_bail("I was kind of expecting a job ID as part of the URL. "
            . "Say, how'd you get here in the first place?");

    error_log("A user running $proper made it to 'display.php' without a "
              . " $jobid.\n\n You /might/ wanna look into this.\n",
              1, $cfg['global']['admin_email']);
} else {
    $jobid = $_GET['jid'];
}

if (!file_exists($INI_FILE)) tf_bail("ACK! Missing or corrupted $INI_FILE.");
if ( !tf_read_config($cfg, $INI_FILE, array( 'job_id' => $jobid)) ) {
    tf_bail("Problem reading $INI_FILE.");
}

// Genome Browser custom track details:
//$HGSID    = '441433011_al4yawdC7UT4zsPZOZFha386RA01';
$HGSID    = $cfg['ucsc']['custom_track_id'];
$UCSCBASE = 'http://genome.ucsc.edu/cgi-bin/hgTracks?db=hub_10649_araTha1&' .
            'hgsid=' . $HGSID;

$output_path = $cfg[$TOOLNAME]['output_path'];
$proper      = $cfg['global']['proper_name'];

tf_template_init(); // instantiate Twig objects

// For 'dev' server only: if the 'jid' URL parameter was "last", try to get the
// last output directory in $output_path:
if ($IS_DEV && $jobid === "last") {
      // Remove the "fake" job ID that got tacked on when the .ini file was 
      // parsed:
      $output_path = str_replace('/last', '', $output_path);
      $jobid = tf_get_last_job_id($output_path);
      if ($jobid === false) {
          tf_bail("Whoops. I ran into problems trying to retrieve the last "
                  . " job ID. Perhaps you should inform the administrator.");
      }
      // Append the real job ID that we (hopefully) just found
      $output_path .= "/$jobid";
} // if URL param jid=last


// Start filling in template variables that we already know:
global $TWIGVARS;

// Can't just assign an array to $TWIGVARS here because we'll clobber useful 
// defaults from tf_template_init(). The right way to fix it is probably with 
// an OO interface like '$TFApp->add_template_vars(...)'.
$TWIGVARS['toolname']    = $TOOLNAME;
$TWIGVARS['propername']  = $proper;
$TWIGVARS['jobid']       = $jobid;
$TWIGVARS['jobname']     = '';
$TWIGVARS['outputfiles'] = '';  // these three get filled in below
$TWIGVARS['results']     = '';  // " "
$TWIGVARS['cmdoutput']   = '';  // " "
$TWIGVARS['gbtrackid']   = $HGSID;
$TWIGVARS['pagetitle']   = "$proper job #$jobid - results";
$TWIGVARS['headerh1']    = <<<END_OF_HTML
  <a href="/$TOOLNAME" title="Back to TF Tools $proper">$proper</a> output for job $jobid
END_OF_HTML;


// Capture job attributes as variables
$jobpropfile = "$output_path/" . $cfg['global']['job_props'];
$lsfjobid    = 0;
$lsfoutput   = '';
$filelist    = preg_split('/\s*,\s*/', $cfg[$TOOLNAME]['output_files']);
$resultsfile = "$output_path/" . $filelist[0];
$results     = '';
$cmdoutput   = '';

// FIXME: Should be using get_job_props() from tfclusterjob.php here
if ( !file_exists($jobpropfile) ) {
    tf_render_error("Could not find any evidence of a $proper job #$jobid. " .
                    "Sorry it didn't work out.");
    error_log("User attempted to access $proper job #$jobid's results, but no "
              . "job properties file ($jobpropfile) could be found.", 
              1, $cfg['global']['admin_email']);
    exit;
} // Otherwise...
$jobdetails  = file($jobpropfile);

// We check for $jobdetails having zero size below, but I guess we should keep 
// plugging  along... The "Undefined offset" notices were getting annoying.
$jobname     = array_key_exists(0, $jobdetails) ? rtrim($jobdetails[0])
                                                : "UNKNOWN JOB #$jobid";
$lsfjobid    = array_key_exists(1, $jobdetails) ? rtrim($jobdetails[1])
                                                : '_NEVER_RAN';
$toollog     = $cfg['cluster']['bsub_log_path'] . "/job$lsfjobid.out";
$toolerr     = "$output_path/bsub_logs/job$lsfjobid.err";

$TWIGVARS['jobname'] = $jobname;


// ====================================================================
//                  C h e c k    t h e    c a c h e
// ====================================================================
//
// Where to pre-render and store the results table if the filesystem hasn't
// changed
$cachefilelist = "./templates/cache/filelist_job$jobid.html";
$cacheresults  = "./templates/cache/results_job$jobid.html";
$cacheoutput   = "./templates/cache/output_job$jobid.html";
$cacheextviews = "./templates/cache/extviews_job$jobid.html";

// If cached results exist for the file list, results table, & the 
// command-line output, and $cacheresults is newer than the output directory, 
// skip traversing the filesystem, just dump the pre-rendered page content.
if (!array_key_exists('purge', $_GET) && file_exists($cachefilelist) &&
    file_exists($cacheresults) && file_exists($cacheoutput) && 
    !dir_changed_since($output_path, $cacheresults)) {

    $TWIGVARS['outputfiles'] = "<!-- retrieved from cache -->\n"
                             . file_get_contents($cachefilelist);
    $TWIGVARS['results']     = file_get_contents($cacheresults);
    $TWIGVARS['cmdoutput']   = file_get_contents($cacheoutput);

    if (file_exists($cacheextviews)) {  
        $TWIGVARS['haveextviews']  = true;
        $TWIGVARS['externalviews'] = file_get_contents($cacheextviews);
    }

    echo $TWIG->render($TEMPLATE, $TWIGVARS);
    exit;
} // if cache is fresh


// ====================================================================
//         S e e   w h e t h e r   j o b   a b o r t e d
// ====================================================================
//
// The presence of 'job_name.txt' in the output directory means that the
// job got submitted to LSF by snpsea.cgi. If the results file isn't there 
// yet, keep waiting.
if (filesize($jobpropfile) > 0 && $lsfjobid != "_NEVER_RAN") {
    // If the LSF job hasn't completed (didn't write its stdout log), then 
    // keep waiting
    if ( !file_exists($toollog) ) {
        tf_refresh_page("/$TOOLNAME/results/$jobid");
        exit;
    } 
} else {
    tf_render_error("Could not find any evidence of a $proper job #$jobid. " .
                    "Sorry it didn't work out.");

    error_log("$proper job #$jobid wrote an empty job properties file, or "
              . "else it never made it to the cluster.", 
              1, $cfg['global']['admin_email']);
    exit;
} // if $jobpropfile exists

// If the job properties file got written, AND we have the LSF log file, BUT 
// we don't have the expected output, then emit an error here and bail out:
if ( !file_exists($resultsfile) ) {
    tf_render_error("It looks like job #$jobid aborted without producing "
                    . "output. Sorry it didn't work out.");

    error_log("$proper job #$jobid failed to produce results ($resultsfile).",
              1, $cfg['global']['admin_email']);
    exit;
}

// Otherwise, create a naïve cache file and write to that.
$fh = fopen($cachefilelist, 'w') or tf_bail("Error creating $cachefilelist.");

// ====================================================================
//         C r e a t e   l i s t   o f   o u t p u t   f i l e s
// ====================================================================
//
// Base query string that we'll build up hyperlinks from
//$qs = 'view.php?t=$TOOLNAME&j=${jobid}"; // now use pretty URLs
$qs = "/$TOOLNAME/$jobid";

// FIXME: Too much presentation in here. These should be their own Twig 
// templates. (See tftools-php-libraries #3 on Bitbucket.)

// Unordered list of filenames (styled in CSS).
// First the  LSF output:
fwrite($fh, '    <ul class="filelist">');
if ($IS_DEV) {
    fwrite($fh, <<<"HTML"
        <li>
          <a href="$qs/bsub_out"><i class="fa fa-info-circle"></i></a>
          <a href="$qs/bsub_out">LSF job output</a>
          <a href="$qs/bsub_out?download" title="Download this file">
            <i class="fa fa-download"></i></a>
        </li>
        <li>
          <a href="$qs/bsub_err"><i class="fa fa-warning"></i></a>
          <a href="$qs/bsub_err">LSF job output (stderr)</a>
          <a href="$qs/bsub_err?download" title="Download this file">
            <i class="fa fa-download"></i></a>
        </li>
HTML
    );
} // don't show LSF output on the public server

// Then the list of files read from the INI:
$hasheader = '';
foreach ($filelist as $file) {
    if (!file_exists("$output_path/$file")) { continue; }

    // For any files that already have a header row (not many output files from 
    // our tools do, but PBMResults is an exception), you can add "&hasheader" 
    // as a URL parameter and render_table() will make one up for you. 
    // PBMresults.bed is an exception, because 'view.php' already had to be 
    // modified to handle this file specially (to add links to TF pages on 
    // CisBP).
    $urlparams = '?render';
    $fa_class   = 'fa-table';

    if ($file === 'PBMresults.bed') {
        $urlparams .= "&unconstrained";
        $HAVE_PBMRESULTS = true;
    }

    if (substr($file, -4) === '.lst') {
        $fa_class = 'fa-file-text-o';
        $urlparams = ''; // don't render these as an HTML table
    }
        
    fwrite($fh, <<<"HTML"
        <li>
          <a href="$qs/$file$urlparams"><i class="fa $fa_class"></i></a>
          <a href="$qs/$file$urlparams">$file</a>
          <a href="$qs/$file?download" title="Download $file">
            <i class="fa fa-download"></i></a>
        </li>
HTML
    );
} // for each file in the list of output files

// Finish of the list of files
fwrite($fh, "      </ul>\n");
fclose($fh); // close the output file list cache file


// ====================================================================
//     C r e a t e   t h e   e x t e r n a l   v i e w   l i n k s
// ====================================================================

// Open the cache file for the links to external views
// FIXME: This whole mess should really be in the Twig template
$motif_logo_hint = '';
if ($HAVE_PBMRESULTS) {
    $motif_logo_hint = <<<"HTML"

            <li>You can view logos for TF binding motifs on CisBP by clicking 
            on motif IDs in the "PWM" column in
            <a href="/$TOOLNAME/$jobid/PBMresults.bed?render&unconstrained"
            >PBMresults.bed</a>.</li>
HTML;
} // if we found 'PBMresults.bed' among the output files (above)

$fh = fopen($cacheextviews, 'w') or bail("Error creating $cacheextviews.");
fwrite($fh, <<<"HTML"
        <ul>
          <li><a href="$UCSCBASE&position=chr1:1-6000"
            title="View UCSC Genome Browser CressInt custom track">
            GB CressInt track</a> (at chr1:1-6000)
          </li>
          <li><a href="http://cisbp.ccbr.utoronto.ca/">CisBP</a></li>
        </ul>

        <div style="color:grey">
          <p><strong>Hints:</strong></p>
          <ul>
            <li>On low-resolution displays, try zooming out once or twice 
            (Ctrl or Command&nbsp;+&nbsp;'-') for the best view of the data in 
            the results table.</li>

            <li>Clicking on <tt>Chr</tt>/<tt>From</tt>/<tt>To</tt> cells in 
            the intersection results below will load the CressInt custom track 
            on the UCSC Genome Browser, displaying &plusmn;3 Kbases around the 
            SNP or sequence.</li>
            $motif_logo_hint
          </ul>
        </div>
HTML
);
fclose($fh);
$TWIGVARS['haveextviews']  = true;
$TWIGVARS['externalviews'] = file_get_contents($cacheextviews);


// ====================================================================
//         C r e a t e   t h e   r e s u l t s   t a b l e
// ====================================================================
//

// Write out the results from 'allresults.bed'
$headers = array(
    "Chr:Input data chromosome",
    "From:Input data 'from' coordinate",
    "To:Input data 'to' coordinate",
    "ID:CressInt's internal SNP ID",
    "Score",
    "Strand",
    "Cell Type",
    "Data Type",
    "Track Chr:Chromosome in dataset intersection",
    "Track From:Dataset intersection 'from' coordinate",
    "Track To:Dataset intersection 'to' coordinate'",
    "Track ID:Gene id from dataset intersection",
    "Track Score",
    "Track Strand"
);
// Add headers for "nearest gene" info that one of the reviewers wanted:
$headers = array_merge($headers, array(
    "Near Dist:Distance to transcription start site of nearest gene",
    "Near Dir:Direction to nearest gene's transcription start site",
    "Near ID:ID of nearest gene",
    "Near Name:Name of nearest gene",
    "Near Strand:Strand of nearest gene"
));

// Open cache file for results table
$fh = fopen($cacheresults, 'w') or bail("Error creating $cacheresults.");
fwrite($fh, <<<"HTML"

    <table id="results-table" class="display">
      <thead>
        <tr>
HTML
);

// Write out the header row, splitting off the part after the colon to become 
// the 'title=' attribute.
foreach ($headers as $h) {
    $headertip = split(':', $h);
    // Just use the column label if no tip is provided, so all have a tooltip
    $headertip[1] = $headertip[1] ?: $headertip[0];
    fwrite($fh, "<th title=\"${headertip[1]}\">${headertip[0]}</th>");
}
fwrite($fh, <<<"HTML"
</tr>
      </thead>
      <tbody>

HTML
);

// Actually (try) to open the results file.
// FIXME: Twig can do iterations in template syntax, so this really should be 
// passed in as an array, rather than raw HTML.
$cols = count($headers);
if (($fp = fopen($resultsfile, "r")) !== FALSE ) {
    // CressInt's 'allresults.bed' doesn't have a header row
    //$data = fgetcsv($fp, 0, "\t"); // throw header row away

    while (($data = fgetcsv($fp, 0, "\t")) !== FALSE) {
      fwrite($fh, indent_it(pad_table_row($cols, $data), 8));
    } // while we have data

    fclose($fp);
} else {
    fwrite($fh, "<tr><td colspan=\"$cols\">Job yielded no results</td></tr>\n");
} // for each row in the results file

// Finish off the table
fwrite($fh, <<<"HTML"
      </tbody>
    </table>
HTML
);

// Close the cache file
fclose($fh);


// ====================================================================
//         R e n d e r   c o m m a n d - l i n e   o u t p u t
// ====================================================================

// Open the cache file
$fh = fopen($cacheoutput, 'w') or tf_bail("Error creating $cacheoutput.");

// Display the LSF output/error logs
// Do stdout first
if (file_exists($toollog)) {
    if (filesize($toollog) == 0) {
        fwrite($fh, get_tf_callout( array (
            'priority' => E_NOTICE,
            'message'  => "Tool didn't generate any output",
            'detail'   => "Looks like <tt>$TOOLNAME</tt> didn't " .
            "generate any output at the the command line." ))
        );
    } else {
        $out = file_get_contents($toollog);
        fwrite($fh, get_tf_callout( array (
            'priority' => E_NOTICE,
            'message'  => "Standard output for LSF job #$lsfjobid",
            'detail'   => "<code>$out</code>" ))
        );
    } // if the log file was empty
} else {
    fwrite($fh, get_tf_callout( array(
        'priority' => E_WARNING,
        'detail'   => 'Tool log file not found.' ))
    );
} // if we found a log file (even empty)

// Now stderr, if it exists:
if (file_exists($toolerr)) {
    if (filesize($toolerr) == 0) {
        fwrite($fh, get_tf_callout( array (
            'priority' => E_NOTICE,
            'message'  => "Tool didn't generate any errors",
            'detail'   => "Looks like the job ran without errors."  ))
        );
    } else {
        $err = file_get_contents($toolerr);
        fwrite($fh, get_tf_callout( array (
            'priority' => E_ERROR,
            'message'  => "Standard error for LSF job #$lsfjobid",
            'detail'   => "<code>$err</code>" ))
        );
    } // if the error log file was empty
} else {
    fwrite($fh, get_tf_callout( array(
        'priority' => E_WARNING,
        'detail'   => 'Tool error file not found.' ))
    );
} // if we found a log file (even empty)

// Close the command-line output cache file
fclose($fh);

// ====================================================================
//          W r i t e   o u t   w h a t   w e ' v e   g o t
// ====================================================================

$TWIGVARS['outputfiles'] = file_get_contents($cachefilelist);
$TWIGVARS['results']     = file_get_contents($cacheresults);
$TWIGVARS['cmdoutput']   = file_get_contents($cacheoutput);

echo $TWIG->render($TEMPLATE, $TWIGVARS);

// vi: sw=4 ts=4 expandtab textwidth=78 tags+=/var/www/lib/php/WRL/tags
// CressInt's display.php
