{#################################################
 #
 #    
 #    Twig template for Cressint Tools FAQs
 #    
 #
 #    Authors:    K. Ernst, F. Soman
 #
 #################################################}
 {# /var/www/twig/tftool.html.twig #}
{% extends 'tftool.html.twig' %}

{% block headassets %}
{# All the regular CSS and JavaScript assets #}
{{ parent() }}
{# Input form-specific CSS and JavaScript (Formalize, sampledata.js, etc.) #}
  <link rel="stylesheet" type="text/css" href="css/cressint.css">    
{% endblock %}

{% block nav %}
{% include 'nav.html.twig' %}
{% endblock %}

{% block main %}
  <div class="info-list">
    <div id="right-toc" style="display:none">
      <h4>Table of Contents</h4>
      <ul id="toc-list" style="">
      </ul>
    </div>
    <h1>Frequently Asked Questions</h1>
    <ol>
      <li><dl>
        {# FIXME: this should be a real ordered list #}
        <dt>What is CressInt?</dt>
        <dd>
          <p><em>CressInt</em> is a web resource for exploring gene regulatory
          mechanisms in <em><a href="https://www.arabidopsis.org/portals/education/aboutarabidopsis.jsp"
          >A. thaliana</a></em> on a genomic scale. CressInt incorporates a
          variety of genome-wide data types relevant to gene regulation,
          including transcription factor (TF) binding site models, ChIP-seq,
          DNase-seq, eQTLs, and GWAS. Examples uses of CressInt are: (1)
          Identify TFs binding to the promoter of a gene of interest; (2)
          identify genetic variants that are likely to impact TF binding based
          on a ChIP-seq dataset; and (3) identify specific TFs whose binding
          might be impacted by phenotype-associated variants.</p>

          <p>The manuscript associated with <em>CressInt</em> was submitted
          for publication to <em>Current Plant Biology</em> in September 2015,
          and may be <a href="http://www.sciencedirect.com/science/article/pii/S2214662815000110?np=y">
          read online</a>.

          <p>A public web interface for CressInt is freely available at <a
          title="CressInt public web interface" href="https://cressint.cchmc.org">https://cressint.cchmc.org</a>.
          Source code for the front-end web application (written in PHP) is
          available on <a title="CressInt source code repository on Bitbucket"
          href="https://bitbucket.org/weirauchlab/tf-tools-cressint">Bitbucket</a>.
          <p>
        </dd>
      </dl></li>

      <li><dl>
        <dt>How should I cite CressInt?</dt>
        <dd>
          <p>
            The manuscript associated with <em>CressInt</em> (X. Chen, <em>et
            al.</em>, <em>CressInt</em>: A user-friendly web resource for
            genome-scale exploration of gene regulation in <em>Arabidopsis
            thaliana</em>, <a href="http://dx.doi.org/10.1016/j.cpb.2015.09.001">dx.doi.org/10.1016/j.cpb.2015.09.001</a>)
            was published in the 15 September 2015 issue of <em>Current Plant
            Biology</em>.
          </p>
          <p>Citation information may be found <a href="/cressint/cite.php">here</a>.</p>
        </dd>
      </dl></li>

      <li><dl>
        <dt>What types of input will CressInt accept?</dt>
        <dd>
          <p>CressInt accepts standard <a title="BED format reference at genome.ucsc.edu"
          href="https://genome.ucsc.edu/FAQ/FAQformat.html#format1"
          >BED file</a> input for genomic coordinates (described in more
          detail below) as well as coordinates of the form
          <tt>chr1:1-10,000,000</tt>, which you might want to copy-paste
          from the UCSC Genome browser.</p>

          <p>With the "Input type: Gene names" radio button selected, you may
          use the search box above the input field to find <em>A.
            thaliana</em> gene names from a comprehensive list (including
          aliases). You may also copy-and-paste a list of gene names directly
          into the input field, one per line. Letter case is not
          significant.</p>

          <p>In both cases, you also have the option of uploading a file
          containing the genomic coordinates or gene names. Both MS-DOS
          and Unix-style <a title="'Newline' article on Wikipedia"
          href="https://en.wikipedia.org/wiki/Newline">line endings</a> are
          accepted.</p>

          <p>Work is in progress to add a <a
          title="FASTA format reference at zhanglab.ccmb.med.umich.edu"
          href="http://zhanglab.ccmb.med.umich.edu/FASTA/">FASTA</a>
          input option.</p>
        </dd>
      </dl></li> <!-- What types of input will CressInt accept -->

      <li><dl>
        <dt>What is a BED file?</dt>
        <dd><p>The BED (or <em><a href="https://genome.ucsc.edu/FAQ/FAQformat.html#format1"
        title="BED format reference at genome.ucsc.edu">Browser
        Extensible Data</a></em>) format describes a tab-delimited text file
        which contains information about the locations on chromosomes of
        regions of interest.</p>

        <p>The minimum recommended input for use with CressInt is BED4 format,
        comprised of the following fields:</p>
        <ol>
          <li><strong><tt>chrom</tt></strong> – the name of the chromosome or scaffold (example:
            <tt>chr5, or scaffold10671</tt>)</li>
          <li><strong><tt>chromStart</tt></strong> – the starting position of feature in the
            chromosome or scaffold</li>
          <li><strong><tt>chromEnd</tt></strong> – the ending position of the feature in the
            chromosome or scaffold</li>
          <li><strong><tt>name</tt></strong> – a name or reference ID for this
            coordinate (don't use spaces in this input field)</li>
        </ol>

        <p>Example: <code>chr1    11873    14409    uc001aaa.3</code></p>
       
        <p>Currently, BED 3, BED 4, BED 5, or BED 6 file formats are supported
        in this version of CressInt (all designating the number of columns in
        the input). <em>Missing columns will be supplied with placeholders,
        which is why you may see arbitrary <tt>seq<em>XXX</em></tt> IDs applied
        to your input after submission, and in the intersection
        results.</em></p>
        
        <p>Strictly speaking, BED format requires that individual fields be
        <strong>delimited by "Tab" (<tt>\t</tt>) characters</strong>, but
        CressInt tries to be somewhat liberal and will generally accept any
        number of whitespace characters as a field delimiter. <em>Don't use
        spaces within any of the optional fields (such as "name") to avoid
        problems processing your input.</em></p>

        <ul style="list-style-type:none">
          <li>
            <p>A <strong>BED5 file</strong> has five attribute columns including score:
              <tt>chrom</tt>, <tt>start</tt>, <tt>end</tt>, <tt>name</tt>, and
              <tt>score</tt>.</p>

              <p>Example: <code>chr1    11873    14409    uc001aaa.3    0</code></p>
            </li>

          <li>
            <p>A <strong>BED6 file</strong> has six attribute columns including
              score and strand: <tt>chrom</tt>, <tt>start</tt>, <tt>end</tt>,
              <tt>name</tt>, <tt>score</tt>, and <tt>strand</tt>.</p>
              
              <p>Example: <code>chr1    11873    14409    uc001aaa.3    0    +</code></p>
          </li>
        </ul>
        </dd>
      </dl><li> <!-- What is a BED file -->

      <li><dl>
        <dt>What is a Protein Binding Microarray?</dt>
        <dd><p>PBMs were originally developed by Martha Bulyk, and have since
        been adopted by many other groups. PBMs are unique because they offer
        an unbiased survey of the binding of a given protein. Briefly, PBMs
        contain ~40,000 double-stranded 60-base DNA probes, which are used to
        systematically measure the binding preferences to all possible DNA
        sequences – the probe sequences of a given PBM array are designed such
        that each of the 32,896 possible 8-base sequences appear in diverse
        flanking sequence contexts on 32 different probes. The resulting data,
        which track well with both in vivo-derived motifs and motifs derived
        from other in vitro assays, therefore offer a complete, robust,
        unbiased survey of the binding preferences of a given TF.</p></dd>
      </dl></li> <!-- What is a PBM? -->

      <li><dl>
        <dt>What should the output of CressInt look like?</dt>
        <dd>
          <p>Operation in "Intersect" mode (Mode 1) should produce just one
          results file:
          <tt><a href="sample_output/mode1/allresults.bed">allresults.bed</a></tt>,
          which represents intersections of your provided gene coordinates
          with all available CressInt data sets.</p>

          <p>In "Find TF/SNPs" mode (Mode 2) and "Phenotypes to TF/SNPs" mode
          (Mode 3), you should get three results files: <tt>allresults.bed</tt>,
          <tt>PBMresults.bed</tt>, and <tt>SNPoutput.bed</tt>. <em>Samples:
          <a href="sample_output/mode2">Mode 2</a> and
          <a href="sample_output/mode3">Mode 3</a></em>.</p>

          <p>Other files incidental to the analysis may be offered for
          download, such as the list of input coordinates you initially
          provided or phenotype selections for Mode 3.</p>
       </dd>
      </dl></li> <!-- How to obtain FASTA files -->

      <li><dl>
        <dt>What does "Mode 1" (or 2 or 3) mean?</dt>
        <dd>
          <p>CressInt job results will refer to completed analysis as being
          one of three "modes." These correspond directly to the three radio
          buttons on the input form:</p>
          <ul>
            <li><strong>Mode 1</strong> (Intersect): Just intersect input
              coordinates with selected datsets</li>
            <li><strong>Mode 2</strong> (Find TF/SNPs): Identify SNPs within
              input coordinates, and the transcription factors whose binding
              they might affect.</li>
            <li><strong>Mode 3</strong> (Phenotypes to TF/SNPs): Identify SNPs
              associated with phenotypes of interest, and the transcription
              factors whose binding they might affect.</li>
          </ul>
        </dd>
      </dl></li>

      <li><dl>
        <dt>What does the "Unconstrain table" button do, exactly?</dt>
        <dd>
          <p>
            The precise layout of tables in web pages is notoriously hard to
            control using just plain HTML. After analysis results are
            downloaded to your browser, a bit of JavaScript is used to
            compute optimal widths for all of the table columns, so that the
            entire table can be viewed within the "viewport" of your browser
            window. This, however, causes many cells' contents to be
            truncated, so a a small popup ("tooltip") over each cell will show
            you the untruncated value.
          </p>
          <p>
            Clicking the "Unconstrain table" button removes this constraint,
            allowing all table cells to go back to their original size. This
            will likely require that you use the horizontal scroll bars in
            order to see all the remaining columns which flow off into the
            right margin.
          </p>
        </dd>
      </dl></li>

      <li id="contact"><dl>
        <dt>How may I contact the developers?</dt>
        <dd>
          <p>See something that needs improved? Let us know.</p>

          <p>You may contact Dr. Matthew Weirauch with questions or feedback
          at <a href="mailto:Matthew.Weirauch -at- cchmc.org?subject=CressInt%20feedback">
          Matthew.Weirauch -at- cchmc.org</a>. <em>Please don't forget to
          change <tt>-at-</tt> to "<tt>@</tt>" before clicking "Send" in your
          email client.</em>
        </dd>
      </dl></li> <!-- How to contact the lab -->

    </ol>
  </div> <!--/.info-list-->
{% endblock %}

{% block footerjs %}
<script>
  // Dynamically create a TOC from all the <dt> elements on the page
  // (the TOC <div> is not displayed at all if JavaScript is diabled)
  window.addEventListener('load', function() {
    function slugify(text) {
      return text.toLowerCase().replace(/ /g, '-').replace(/[^-a-z0-9]/g, '');
    }

    var forEach = Array.prototype.forEach;
    var tds = document.getElementsByTagName('dt');
    var toc_ul = document.getElementById('toc-list');
    var tocitem, link, slug, title;

    forEach.call(tds, function(term) {
      title = term.textContent;
      slug = slugify(title);
      term.setAttribute('id', slug);
      li = document.createElement('li');
      li.textContent = title;
      link = document.createElement('a');
      link.appendChild(li);
      link.setAttribute('href', '#'+slug);
      toc_ul.appendChild(link);
    });

    document.getElementById('right-toc').style.display = 'inherit';
  });
</script>
{% endblock %}
