var CaseStudy = CaseStudy || (function() {
  var flashintvl = '50ms';
  var flashrpt   = 5;
  var resetbtn   = document.getElementById('resetbtn');
  var samplebtn  = document.getElementById('samplebtn');
  var cs1btn     = document.getElementById('case-study-1');
  var cs2btn     = document.getElementById('case-study-2');
  var cs3btn     = document.getElementById('case-study-3');
  var mode1radio = document.getElementById('mode1-radio');
  var mode2radio = document.getElementById('mode2-radio');
  var mode3radio = document.getElementById('mode3-radio');
  var inputform  = document.getElementById('coords');
  var generadio  = document.getElementById('input-type-names');
  var coordradio = document.getElementById('input-type-coords');
  var promochk   = document.getElementById('scan-promoter');
  var phenolist  = document.getElementById('phenotypes');
  var phenoexpl  = document.getElementById('pheno-explain');
  var upbase     = document.getElementById('up-bases');
  var downbase   = document.getElementById('down-bases');

  // Defaults parameters for the case studies
  var case3pheno     = 'FT_Field';
  var case1upbases   = 3000;
  var case1downbases = 0;

  // Simulate a click event in a way that IE/Chrome/Firefox can all possibly,
  // maybe comprehend. Source: http://stackoverflow.com/a/5159924
  function __simulateClick(e, type) {
    if (typeof(e) === 'undefined') {
      console.log('__selectByValue expects a DOM element');
      return;
    }
    if (typeof(type) === 'undefined') { type = 'click'; }
    var trigger;
    // Check for createEventObject
    if (document.createEventObject){
        // Trigger for Internet Explorer
        trigger = document.createEventObject();
        e.fireEvent('on' + type, trigger);
    }
    else {
        // Trigger for the good browsers (WTF, Chrome?)
        //trigger = document.createEvent('HTMLEvents');
        //trigger.initEvent(type, true, true);
        //e.dispatchEvent(trigger);
        e.click();
    }
  } // __simulateClick
  
  // Completely clear a mult-select form control.
  function __clearMultiSelect(e) {
    for(var i = 0; i < e.options.length; i++){
      e.options[i].selected = false;
    }
  }

  // Select an individual element in a multi-select input element, leaving all
  // the other options de-selected. Returns the index of the matched item,
  // null if there was an error, or -1 if no match was found.
  function __selectByValue(e, val) {
    if (typeof(e) === 'undefined' || e.tagName !== 'SELECT') {
      console.log('__selectByValue expects a <select> element');
      return null;
    }
    // Deselect *everything* first:
    __clearMultiSelect(e);

    // Iterate over element's children until we find the one matching "val"
    for (var i = 0; i < e.options.length; i++) {
      if (e.options[i].value === val) {
        e.options[i].selected = true;
        return i;
      }
    } // otherwise...
    return -1;
  } // __selectByValue

  function __confirmClearInput(e) {
    if (e.value !== '' &&
        !confirm("This will clear the input list's contents. Is that okay?")) {
      return false;
    } // otherwise
    e.value = '';
    return true;
  }

  // Doesn't work just yet (2015-08-16)
  function __flashElement(e) {
    e.style.transition = 'opacity ' + flashintvl;
    var intvl = window.setInterval(function() {
      e.style.opacity = e.style.opacity === '1' ? '0' : '1'; }, flashintvl);

    var stop = (function(i){return function(){window.clearInterval(i);};}(intvl));
    var timer = window.setTimeout(stop, flashintvl * flashrpt);
  }


  // ====================================================================
  //                     P U B L I C   E X P O R T S
  // ====================================================================
  return {
    wireUp: function() {
      cs1btn.disabled = false; cs1btn.onclick = this.loadCS1;
      cs2btn.disabled = false; cs2btn.onclick = this.loadCS2;
      cs3btn.disabled = false; cs3btn.onclick = this.loadCS3;
    },

    // From the manuscript (p. 11 in the PDF version)
    // > To identify possible candidates, the user simply enters “AGL20” and
    // > defines the desired promoter search space (for this example, we use
    // > the region starting at the AGL20 TSS and extending 1000 bases
    // > upstream).
    loadCS1: function() {
      if (!__confirmClearInput(inputform)) { return; }
      __simulateClick(resetbtn);

      // FIXME: Hard-coded delays are not the right way to do this.
      window.setTimeout(
        function() { __simulateClick(mode2radio); }, 200);
      window.setTimeout(
        function() { __simulateClick(generadio); }, 200);
      window.setTimeout(
        function() { __simulateClick(promochk); }, 300);
      window.setTimeout(
        function() { __simulateClick(samplebtn); }, 300);

      upbase.value   = case1upbases;
      downbase.value = case1downbases;
    },

    // From the manuscript (p. 11 in the PDF)
    // > To demonstrate how CressInt can generate specific hypotheses from a
    // > user-provided genome- wide dataset, we submitted PIF1/PIL5 ChIP-seq
    // > peak regions in seedlings to CressInt as input, and asked the system
    // > to identify all likely PIF1 binding sites within these peaks that
    // > overlap naturally occurring genetic variants.
    loadCS2: function() {
      if (!__confirmClearInput(inputform)) { return; }
      __simulateClick(resetbtn);

      window.setTimeout(
        function() { __simulateClick(mode2radio); }, 200);
      window.setTimeout(
        function() { __simulateClick(coordradio); }, 200);
      window.setTimeout(
        function() { __simulateClick(samplebtn); }, 300);
    },

    // From the manuscript (p. 13)
    // > This user would start by selecting relevant phenotypes (e.g.
    // > “FT_field”, which counts the number of days between germination date
    // > and appearance of the first flower).
    loadCS3: function() {
      if (!__confirmClearInput(inputform)) { return; }
      __simulateClick(resetbtn);

      window.setTimeout(
        function() { __simulateClick(mode3radio); }, 200);
      window.setTimeout(
        function() {
          var i = __selectByValue(phenolist, case3pheno);
          // Update the phenotype explanation as well
          msg = '<strong>' + phenolist.children[i].value + ':</strong> ' +
                phenolist.children[i].title;
          phenoexpl.innerHTML = '<p>' +msg+ '</p>';
        }, 300);
    }
  }; // public exports
}());
