// -----------------------------------------------------------------
//
//   @file formsubmit.js
//
//   @brief Functions to support form submission (and feedback on
//   validation errors) via Ajax.
//
//   @author  Kevin Ernst (ern6xv)
//   @date    XX March 2014
//
// -----------------------------------------------------------------
'use strict';

var alerttimer = null;

function doAlert(alertbox, heading, msg) {
  alertbox.innerHTML = '<h4>' +heading+ "</h4>\n" + '<p>' +msg+ "</p>\n";
  alertbox.classList.remove('concealed');
  alertbox.classList.add('shown');
} // doAlert

function dismissAlert(alertbox) {
  alertbox.classList.remove('shown');
  alertbox.classList.add('concealed');
} // dismissAlert

function doAutoDismissAlert(alertbox, heading, msg, timer) {
  if (typeof(timer) === 'undefined') { timer = 5000; }
  if (alerttimer) {
    window.clearTimeout(alerttimer);
    alerttimer = null;
  }

  // Clear the message after a delay
  alerttimer = window.setTimeout(function(alertbox) {
    dismissAlert(alertbox);
  }, timer);
  doAlert(alertbox, heading, msg);
} // doAutoDismissAlert

function anchorList(anchors, msg, mouseover) {
    if (typeof(mouseover) === 'undefined') {
      mouseover = 'Jump to this location on the page';
    }
    var anchor;
    var html = "<div class=\"inline-list\">\n" +
               "  " +msg+ "\n" +
               "  <ul class=\"inline-list\">\n";
    // Create an unordered list clickable links for each one of the
    // anchors supplied:
    for (var i = 0; i < anchors.length; i++) {
      anchor = anchors[i];
      html += '    <li><a title="' +mouseover+ '" href="#' +anchor+
              '">' +anchor+ "</a></li>\n";
    }
    html += "  </ul>\n</div>\n";
    return html;
} // anchorList

// Wire up the input form's submit button to use Ajax
// (/assets/js/ajax.js):
WLAjax.tieForm('inputform', function(status, responseText) {
  var alertbox = document.getElementById('valid-msg');

  // Hide the spinner 
  WLAjax.hideSpinner();

  if (status === 200) {
    // Does this browser support the HTML5 History API?
    if (window.history && history.pushState) {
      history.pushState({}, "response");
      // FIXME: This is kind of unfriendly, because it will clobber any
      // existing form values (that kind browsers like Firefox would've
      // retained for you), but it also gets around the problem of two
      // submissions having the same form key, which usually results in
      // the CGI script dying and an Error 500 in the response.
      window.onpopstate = function(e) { document.location.reload(); };
    }
    document.getElementById('main');
    main.innerHTML = responseText;
    return;
  }

  if (status === 500) {
    // This will most likely contain an Apache 404.html, so we DON'T want
    // to display the responseText in this case.
    doAutoDismissAlert(alertbox, 'Uh-oh.',
      "There was a problem submitting the form. Perhaps try " +
      '<a href="#" onclick="document.location.reload();">reloading this ' +
      "page</a>? (Clears all existing inputs!)<br>\n\n" +
      '<small>Guru meditation (' +status+ '): Internal Server Error' +
      "</small>\n", 8000);
    return;
  }

  if (status === 400) {
    var missing  = /missing.*: (.*)/i.exec(responseText);
    var invalid  = /invalid.*: (.*)/i.exec(responseText);
    var parseerr = /problem parsing.*/i.exec(responseText);
    var tooltip  = 'Go to the error on the input form';
    var names = [];
    var param, msg, i;

    if (parseerr) {
      msg = 'There was a problem parsing your <a href="#inputvalues">' +
            'input coordinates</a>. (They should be in tab-delimited BED ' +
            'file format.)';
      doAlert(alertbox, 'Bad input coordinates or gene names', msg);
    }
    else if (missing) {
      // Send the users to the 'inputvalues' anchor if we got either 'coords'
      // or 'phenotypes'
      if ( /(coords|phenotypes)/.exec(missing[1]) ) {
        names = [ 'inputvalues' ];
      } else {
        names = missing[1].split(', ');
      }

      msg = "It looks like you're missing some form inputs:";
      msg = anchorList(names, msg, tooltip);
      doAlert(alertbox, 'Missing parameters', msg);
    }
    else if (invalid) {
      names = invalid[1].split(', ');
      msg = 'Some of your inputs were invalid or out-of-range:';
      msg = anchorList(names, msg, tooltip);
      doAlert(alertbox, 'Invalid parameters', msg);
    }
    else {
      doAutoDismissAlert(alertbox, 'Oops.',
        'There was some problem with your form inputs. Please ' +
        "double-check them and try submitting again.<br>\n\n" +
        '<small>Guru meditation (' +status+ '): ' + responseText +
        "</small>\n", 10000);
    } // if we got back a message about missing form inputs
  } // status 400
}); // WLAjax.tieForm
